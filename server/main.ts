import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import "/imports/startup/server/index"
import {SettingsCollection} from "/imports/api/settings/settings";
import {SettingsConstants} from "/imports/api/settings/constants";
// import {CasesCollection} from "/imports/api/cases/cases";
// @ts-ignore
import {Log} from "meteor/logging";

// TODO change passwords when application is deployed
const usernames = [
    {
        email: 'sherlock@meddling.com',
        password: 'd@ Trvf w!ll 0ut.'
    },
    {
        email: 'winston@meddling.com',
        password: "Gah zuks h3's n0t dEad!"
    }
];

Meteor.startup(() => {

    // TODO remove, only here for the development purposes
    // delete users on start up, again only for development purposes
    /*
    const userCount: number = Meteor.users.find({}).count();
    if (userCount > 0) {
        Meteor.users.remove({});
        Log.info(`Deleted users ${userCount} from db...`)
    }
     */

    // TODO remove after playing around with this for a bit
    // const caseCount: number = CasesCollection.find({}).count()
    // if (caseCount > 0) {
    //     CasesCollection.remove({});
    // }

    try {
        // populate users
        for (let i = 0; i < usernames.length; i++) {
            if (!Accounts.findUserByEmail(usernames[i].email)) {
                Log.info(`creating user ${usernames[i].email}`);
                Accounts.createUser({
                    email: usernames[i].email,
                    password: usernames[i].password,
                });
            } else {
                Log.info(`user already exists: ${usernames[i].email}`)
            }
        }

        let settingsCount: number = SettingsCollection.find().count();

        // TODO delete when promoted to production
        // used as a work around to clean out collection when needed...
        /*
        if (settingsCount > 0) {
            SettingsCollection.remove({});
            settingsCount = 0;
        }
         */

        // populate default settings
        if (SettingsConstants.length > settingsCount) {
            for (let i = 0; SettingsConstants.length > settingsCount; i++) {
                const config: any = SettingsConstants[i];
                // now determine if the id exists in the table
                const setting: any = SettingsCollection.findOne({version: config?.version});
                if (!setting) {
                    SettingsCollection.insert({
                        version: config.version,
                        dateCreated: config.dateCreated,
                        general: config.general,
                        subjects: config.subjects,
                        addresses: config.addresses,
                        vehicles: config.vehicles
                    })
                } else {
                    Log.info(`Setting already exists, setting = ${setting?.toString()}`);
                    break
                }
            }
        }
    } catch (e) {
        // @ts-ignore
        Log.error(`An error occurred: ${e.message}`);
    }
});
