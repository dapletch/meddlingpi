import {Cases} from "/imports/api/cases/cases";
import {Session} from "meteor/session";
import {SessionConstants} from "/imports/ui/constants/SessionConstants";
import {SettingConstants} from "/imports/ui/constants/SettingConstants";
// @ts-ignore
import {Log} from "meteor/logging";

export class CaseUtils {

    public updateCaseRecord(setting: string, config: any, caseRecord: Cases): Cases {
        switch (setting) {
            case 'general':
                caseRecord.general = this.appendCategoryToFieldNames(setting, config);
                caseRecord.subjects = caseRecord?.subjects ? caseRecord.subjects : [];
                caseRecord.addresses = caseRecord?.addresses ? caseRecord.addresses : [];
                caseRecord.vehicles = caseRecord?.vehicles ? caseRecord.vehicles : [];
                break;
            case 'subjects':
                caseRecord.general = caseRecord?.general ? caseRecord.general : [];
                caseRecord.subjects = this.appendCategoryToFieldNames(setting, config);
                caseRecord.addresses = caseRecord?.addresses ? caseRecord.addresses : [];
                caseRecord.vehicles = caseRecord?.vehicles ? caseRecord.vehicles : [];
                break;
            case 'addresses':
                caseRecord.general = caseRecord?.general ? caseRecord.general : [];
                caseRecord.subjects = caseRecord?.subjects ? caseRecord.subjects : [];
                caseRecord.addresses = this.appendCategoryToFieldNames(setting, config);
                caseRecord.vehicles = caseRecord?.vehicles ? caseRecord.vehicles : [];
                break;
            case 'vehicles':
                caseRecord.general = caseRecord?.general ? caseRecord.general : [];
                caseRecord.subjects = caseRecord?.subjects ? caseRecord.subjects : [];
                caseRecord.addresses = caseRecord?.addresses ? caseRecord.addresses : [];
                caseRecord.vehicles = this.appendCategoryToFieldNames(setting, config);
                break;
        }
        return caseRecord;
    }

    public saveCaseInSession(caseRecord: any): void {
        var encode = require('hashcode').hashCode;
        if (caseRecord) {
            if (Session.get(SessionConstants.CASE_RECORD)) {
                if (encode().value(caseRecord) != encode().value(Session.get(SessionConstants.CASE_RECORD))) {
                    Session.set(SessionConstants.CASE_RECORD, caseRecord);
                }
            } else {
                Session.set(SessionConstants.CASE_RECORD, caseRecord);
            }
        }
    }

    public saveCasesInSession(caseIds: Array<any>): void {
        var encode = require('hashcode').hashCode;
        if (caseIds?.length > 0) {
            if (Session.get(SessionConstants.CASE_LIST)) {
                if (encode().value(caseIds) != encode().value(Session.get(SessionConstants.CASE_LIST))) {
                    Session.set(SessionConstants.CASE_LIST, caseIds);
                }
            } else {
                Session.set(SessionConstants.CASE_LIST, caseIds);
            }
        }
    }

    public setDefaultSetting(): void {
        if (!Session.get(SessionConstants.CASE_SETTING)) {
            Session.set(SessionConstants.CASE_SETTING, SettingConstants.DEFAULT_SETTING);
        }
    }

    public saveCaseSelection(caseRecord: Cases): number {
        let index: number = 0;
        if (caseRecord) {
            const cases: Array<Cases> = Session.get(SessionConstants.CASE_LIST);
            for (let i: number = 0; i < cases?.length; i++) {
                if (caseRecord.caseId == cases[i].caseId) {
                    // need to have log statement for state of index to persist for some reason...
                    Log.debug("index: " + i);
                    // set the current case index into session to be used as the default when the user accesses the case settings page again.
                    Session.set(SessionConstants.CASE_INDEX, i);
                    index = i;
                    break;
                }
            }
        }
        return index;
    }

    public getSelectedValue(options: Array<any>): any {
        let defaultValue: any = {};
        if (options?.length > 0) {
            const dropDownIndex: number = options.map((e) => {
                return e.selected;
            }).indexOf(true);
            Log.debug("dropDownIndex: " + dropDownIndex)
            defaultValue = options[dropDownIndex];
        }
        return defaultValue;
    }

    public createOptionIds(category: string, name: string, options: Array<any>): Array<string> {
        let ids: Array<string> = [];
        if (options?.length > 0) {
            for (let i: number = 0; i < options?.length; i++) {
                ids.push(`${category}_${name}_${i}`)
            }
        }
        return ids;
    }

    public appendCategoryToFieldNames(category: string, formData: Array<any>): Array<any> {
        let list: Array<any> = [];
        for (let i: number = 0; i < formData?.length; i++) {
            let formElement: any = formData[i];
            if (formElement?.hasOwnProperty('name')) {
                if (!formElement.name.startsWith(category)) {
                    formElement.name = `${category}_${formElement.name}`;
                    list.push(formElement);
                }
            } else {
                Log.error(`Element doesn't contain name: ${formElement.toString()}`);
            }
        }
        // if no items in the list were modified then just return the original list
        return list.length > 0 ? list : formData;
    }

    public updateCategory(inputArray: Array<any>, originalArray: Array<any>): Array<any> {
        let list: Array<any> = [];
        for (let i: number = 0; i < originalArray?.length ; i++) {
            let input: any = inputArray[i];
            let original: any = originalArray[i];
            // switch case on item type
            const type: string = original.type;
            switch (type) {
                case 'checkbox-group':
                    let checkboxes: Array<any> = original.values;
                    // loop over to update the selected value
                    for (let j: number = 0; j < checkboxes?.length; j++) {
                        checkboxes[j].selected = input.nodes[j].checked;
                    }
                    original.values = checkboxes;
                    break;
                case 'date':
                    original.value = input.nodes[0].value;
                    break;
                case 'hidden':
                    original.value = input.nodes[0].value;
                    break;
                case 'number':
                    original.value = input.nodes[0].value;
                    break;
                case 'radio-group':
                    let radios: Array<any> = original.values;
                    // loop over to update the selected value
                    for (let j: number = 0; j < radios?.length; j++) {
                        radios[j].selected = input.nodes[j].checked;
                    }
                    original.values = radios;
                    break;
                case 'select':
                    original.value = input.nodes[0].value;
                    break;
                case 'text':
                    original.value = input.nodes[0].value;
                    break;
                case 'textarea':
                    original.value = input.nodes[0].value;
                    break;
                default:
                    Log.warn(`Unrecognized type: ${type}, skipping...`);
                    break;
            }
            list.push(original)
        }
        return list;
    }

}