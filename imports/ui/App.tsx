import React, {lazy, Suspense} from 'react';
import {
    BrowserRouter as Router,
    Route,
    Routes
} from "react-router-dom";
import {Login} from "/imports/ui/components/login/Login";
import {List} from "/imports/ui/components/cases/list/List";
import {Create} from "/imports/ui/components/cases/create/Create";
import {ProtectedRoute} from "/imports/ui/ProtectedRoute";
import {Edit} from "/imports/ui/components/cases/edit/Edit";
// @ts-ignore
import {Log} from "meteor/logging";

// included routing for the application via:
// https://guide.meteor.com/react.html#routing
// https://reactrouter.com/
// https://v5.reactrouter.com/web/guides/quick-start
export const App = () => {

    Log.info("App loaded...");

    const Settings = lazy(() =>
        import('./components/cases/settings/Settings').then(({Settings}) => ({default: Settings}))
    );

    return (
        <Router>
            <Routes>
                <Route path="/" element={<Login/>}/>
                <Route
                    path="/cases/list"
                    element={
                        <ProtectedRoute>
                            <List/>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/cases/create"
                    element={
                        <ProtectedRoute>
                            <Create/>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/cases/edit"
                    element={
                        <ProtectedRoute>
                            <Edit/>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/cases/edit/:id"
                    element={
                        <ProtectedRoute>
                            <Edit/>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/cases/settings"
                    element={
                        <ProtectedRoute>
                            <Suspense fallback={<div>Loading...</div>}>
                                <Settings/>
                            </Suspense>
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="/cases/settings/:id"
                    element={
                        <ProtectedRoute>
                            <Suspense fallback={<div>Loading...</div>}>
                                <Settings/>
                            </Suspense>
                        </ProtectedRoute>
                    }
                />
            </Routes>
        </Router>
    );
};
