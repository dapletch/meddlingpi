import {Meteor} from "meteor/meteor";
import React from "react";
import {Navigate} from "react-router-dom";
// @ts-ignore
import {Log} from "meteor/logging";

// @ts-ignore
export const ProtectedRoute = ({children}) => {
    let isAuthenticated: boolean = false;
    try {
        // evaluate whether the userId has been populated or not...
        if (Meteor.userId()) {
            isAuthenticated = true;
        }
    } catch (e) {
        // @ts-ignore
        Log.error(`An error occurred while evaluating the user's session: ${e.message}`)
    }
    return (
        isAuthenticated ? children : <Navigate to="/"/>
    );
};