import React from "react";
import {SideNavigation} from "/imports/ui/components/navigation/SideNavigation";
import {TopNavigation} from "/imports/ui/components/navigation/TopNavigation";
// @ts-ignore
import ScriptTag from 'react-script-tag';

// import React, {useEffect} from "react";
// defining the properties per:
// https://www.carlrippon.com/react-children-with-typescript/
type Props = {
    children?: React.ReactNode
}
export const CaseLayout = ({children}: Props) => {

    return (
        <div className="wrapper d-flex align-items-stretch">
            <SideNavigation/>
            <div id="content" className="p-4 p-md-5">
                <TopNavigation/>
                {children}
            </div>
            <script src={"https://res.cloudinary.com/drl6ldpeo/raw/upload/v1638760938/assets/js/js/jquery.min.js"}/>
            <script src={"https://res.cloudinary.com/drl6ldpeo/raw/upload/v1638760938/assets/js/js/jquery-ui.min.js"}/>
            <script src={"https://res.cloudinary.com/drl6ldpeo/raw/upload/v1638760938/assets/bootstrap/js/bootstrap.min.js"}/>
            <ScriptTag
                isHydrating={true}
                type="text/javascript"
                src="https://res.cloudinary.com/drl6ldpeo/raw/upload/v1638760938/assets/js/js/main.js"
            />
            <script src={"https://res.cloudinary.com/drl6ldpeo/raw/upload/v1638760938/assets/js/js/form-builder.min.js"}/>
        </div>
    );
}