import React from "react";
import {Meteor} from "meteor/meteor";
import {useNavigate} from 'react-router-dom';
import {NavLink} from "react-router-dom";

// TODO include roles for role based navigation...
// https://atmospherejs.com/alanning/roles

export const TopNavigation = () => {

    const navigate = useNavigate();

    // redirect user back to login page
    const logout = () => {
        if (Meteor.userId()) {
            Meteor.logout();
        }
        navigate("/");
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">

                <button type="button" id="sidebarCollapse" className="btn btn-primary">
                    <i className="fa fa-bars"/>
                    <span className="sr-only">Toggle Menu</span>
                </button>
                <button className="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i className="fa fa-bars"/>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="nav navbar-nav ml-auto">
                        { /* TODO Make active link green to stick out */}
                        { /* TODO convert to NavLink */}
                        {/*<li className="nav-item active">*/}
                        {/*    <a className="nav-link" href="#" aria-labelledby="User Administrator">Admin</a>*/}
                        {/*</li>*/}
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/cases/list" aria-labelledby="Case List">
                                Cases
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/cases/settings" aria-labelledby="Case Settings">
                                Settings
                            </NavLink>
                        </li>
                        {/*<li className="nav-item">*/}
                        {/*    <a className="nav-link" href="#" aria-labelledby="Profile">Profile</a>*/}
                        {/*</li>*/}
                        {/*<li className="nav-item">*/}
                        {/*    <a className="nav-link" href="#" aria-labelledby="Manage Subscription">Subscription</a>*/}
                        {/*</li>*/}
                        {/*<li className="nav-item">*/}
                        {/*    <a className="nav-link" href="#" aria-labelledby="Give Feedback">Feedback</a>*/}
                        {/*</li>*/}
                        <li className="nav-item">
                            <a className="nav-link" onClick={logout} aria-labelledby="Logout">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}