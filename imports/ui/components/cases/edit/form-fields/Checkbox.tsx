import React, {useState} from "react";
import {InputLabel} from "@material-ui/core";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
// @ts-ignore
import {Log} from "meteor/logging";

type Props = {
    category: string,
    formElement: any
}
export const Checkbox = ({category, formElement}: Props) => {

    // Major help in terms of dealing with checkboxes per:
    // https://www.freecodecamp.org/news/how-to-work-with-multiple-checkboxes-in-react/
    const [checkedState, setCheckedState] = useState(
        new Array(formElement?.values?.length).fill(false)
    );

    const handleOnChange = (position: number) => {
        Log.info("position: " + position)
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );

        setCheckedState(updatedCheckedState);
    }

    const utils = new CaseUtils();
    const checkboxes: Array<any> = formElement?.values;
    const checkboxIds = utils.createOptionIds(category, formElement?.name, checkboxes);

    return (
        <div>
            <InputLabel>{formElement?.label}</InputLabel>
            {checkboxes.map((value, index) =>
                <div className="form-check" key={index}>
                    {value?.selected === true
                        ? React.createElement(
                            'input',
                            {
                                type: 'checkbox',
                                name: formElement?.name,
                                value: value?.value,
                                className: 'form-check-input',
                                defaultChecked: true,
                                id: checkboxIds[index],
                                onChange: () => {handleOnChange(index)}
                            }
                        )
                        : React.createElement(
                            'input',
                            {
                                type: 'checkbox',
                                name: formElement?.name,
                                value: value?.value,
                                className: 'form-check-input',
                                checked: checkedState[index],
                                id: checkboxIds[index],
                                onChange: () => {handleOnChange(index)}
                            }
                        )
                    }
                    <label className="form-check-label" htmlFor={checkboxIds[index]}>
                        {value?.label}
                    </label>
                </div>
            )}
        </div>
    )
}