import React, {useState} from "react";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
import {InputLabel} from "@material-ui/core";
// @ts-ignore
import {Log} from "meteor/logging";

type Props = {
    category: string,
    formElement: any
}
export const Radio = ({category, formElement}: Props) => {

    // Major help in terms of dealing with checkboxes per:
    // https://www.freecodecamp.org/news/how-to-work-with-multiple-checkboxes-in-react/
    const [checkedState, setCheckedState] = useState(
        new Array(formElement?.values?.length).fill(false)
    );

    const handleOnChange = (position: number) => {
        Log.info("position: " + position)
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );

        setCheckedState(updatedCheckedState);
    }

    const utils = new CaseUtils();
    const radios: Array<any> = formElement?.values;
    const radioButtonIds = utils.createOptionIds(category, formElement?.name, radios);

    return (
        <div>
            <InputLabel>{formElement?.label}</InputLabel>
            {radios.map((value, index) =>
                <div className="form-check" key={index}>
                    {value?.selected === true
                        ? React.createElement(
                            'input',
                            {
                                type: 'radio',
                                name: formElement?.name,
                                value: value?.value,
                                className: 'form-check-input',
                                defaultChecked: true,
                                id: radioButtonIds[index],
                                onChange: () => {handleOnChange(index)}
                            }
                        )
                        : React.createElement(
                            'input',
                            {
                                type: 'radio',
                                name: formElement?.name,
                                value: value?.value,
                                className: 'form-check-input',
                                checked: checkedState[index],
                                id: radioButtonIds[index],
                                onChange: () => {handleOnChange(index)}
                            }
                        )
                    }
                    <label className="form-check-label" htmlFor={radioButtonIds[index]}>
                        {value?.label}
                    </label>
                </div>
            )}
        </div>
    );
}