import React, {lazy, Suspense} from "react";

import {CaseLayout} from "/imports/ui/components/layouts/CaseLayout";
import {useParams} from "react-router-dom";
import {useTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {CasesCollection} from "/imports/api/cases/cases";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
import {SessionConstants} from "/imports/ui/constants/SessionConstants";
import { Session } from "meteor/session";

export const Edit = () => {

    // unique identifier for the user's case
    const { id } = useParams();

    const utils = new CaseUtils();

    const Form = lazy(() =>
        import('./Form').then(({Form}) => ({default: Form}))
    );

    const caseRecord = useTracker(() => {
        Meteor.subscribe('cases.publication.caseRecord');
        let caseRecord = CasesCollection.findOne({_id: id, userId: Meteor.userId()})
        if (!caseRecord) {
            caseRecord = Session.get(SessionConstants.CASE_RECORD);
        }
        return caseRecord;
    });

    utils.saveCaseInSession(caseRecord)

    return (
        <CaseLayout>
            <Suspense fallback={<div>Loading...</div>}>
                {/* @ts-ignore */}
                <Form caseRecord={caseRecord}/>
            </Suspense>
        </CaseLayout>
    );
}