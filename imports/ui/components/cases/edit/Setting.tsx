import React from "react";
import TextField from '@material-ui/core/TextField';
import {InputLabel, MenuItem, Select} from "@material-ui/core";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
import {Radio} from "../edit/form-fields/Radio"
import {Checkbox} from "../edit/form-fields/Checkbox"
// @ts-ignore
import {Log} from "meteor/logging";

type Props = {
    category: string,
    setting: Array<object>
}
export const Setting = ({category, setting}: Props) => {

    const utils = new CaseUtils();

    const buildFormElement = (formElement: any) => {
        Log.debug("form type: " + formElement?.type)
        switch (formElement?.type) {
            case 'text':
                const isTextFieldPopulated: boolean = formElement.value != null;
                if (isTextFieldPopulated) {
                    return (
                        <TextField
                            type={formElement?.type}
                            id={formElement?.name}
                            name={formElement?.name}
                            label={formElement?.label}
                            defaultValue={formElement?.value}
                            variant={"standard"}
                            className={formElement?.className}
                            required={formElement?.required}
                        />
                    )
                } else {
                    return (
                        <TextField
                            type={formElement?.type}
                            id={formElement?.name}
                            name={formElement?.name}
                            label={formElement?.label}
                            variant={"standard"}
                            className={formElement?.className}
                            required={formElement?.required}
                        />
                    )
                }
            case 'date':
                const isDatePopulated: boolean = formElement?.value != null;
                if (isDatePopulated) {
                    return (
                        <div>
                            <br/>
                            <InputLabel>{formElement?.label}</InputLabel>
                            <TextField
                                type={formElement?.type}
                                id={formElement?.name}
                                name={formElement?.name}
                                variant={"standard"}
                                defaultValue={formElement?.value}
                                className={formElement?.className}
                                required={formElement?.required}
                                placeholder={formElement?.placeholder}
                            />
                        </div>
                    )
                } else {
                    return (
                        <div>
                            <br/>
                            <InputLabel>{formElement?.label}</InputLabel>
                            <TextField
                                type={formElement?.type}
                                id={formElement?.name}
                                name={formElement?.name}
                                variant={"standard"}
                                className={formElement?.className}
                                required={formElement?.required}
                                placeholder={formElement?.placeholder}
                            />
                        </div>
                    )
                }
            case 'textarea':
                const isTextAreaPopulated: boolean = formElement?.value != null;
                if (isTextAreaPopulated) {
                    return (
                        <TextField
                            type={formElement?.type}
                            id={formElement?.name}
                            name={formElement?.name}
                            label={formElement?.label?.replace("&amp;", "&")}
                            variant={"standard"}
                            defaultValue={formElement?.value}
                            className={formElement?.className}
                            required={formElement?.required}
                            placeholder={formElement?.placeholder}
                        />
                    )
                } else {
                    return (
                        <TextField
                            type={formElement?.type}
                            id={formElement?.name}
                            name={formElement?.name}
                            label={formElement?.label?.replace("&amp;", "&")}
                            variant={"standard"}
                            className={formElement?.className}
                            required={formElement?.required}
                            placeholder={formElement?.placeholder}
                        />
                    )
                }
            case 'checkbox-group':
                return (
                    <Checkbox
                        category={category}
                        formElement={formElement}
                    />
                )
            case 'radio-group':
                return (
                    <Radio
                        category={category}
                        formElement={formElement}
                    />
                );
            case 'select':
                const options: Array<any> = formElement?.values;
                const defaultValue: any = utils.getSelectedValue(options);
                return (
                    <div>
                        <InputLabel>{formElement?.label}</InputLabel>
                        {
                            defaultValue
                                ?
                                <Select
                                    required={formElement?.required}
                                    className={formElement?.className}
                                    name={formElement?.name} defaultValue={defaultValue?.value}>
                                    {options.map((value, index) =>
                                        <MenuItem
                                            value={value?.value}
                                            selected={value?.selected} key={index}>
                                            {value?.label}
                                        </MenuItem>
                                    )}
                                </Select>
                                :
                                <Select
                                    required={formElement?.required}
                                    className={formElement?.className}
                                    name={formElement?.name}>
                                    {options.map((value, index) =>
                                        <MenuItem
                                            value={value?.value}
                                            selected={value?.selected} key={index}>
                                            {value?.label}
                                        </MenuItem>
                                    )}
                                </Select>
                        }
                    </div>
                )
            case 'hidden':
                return (
                    <input type={"hidden"}
                           name={formElement?.name}
                           value={formElement?.value}
                    />
                )
            case 'paragraph':
                return (
                    <p dangerouslySetInnerHTML={{__html: formElement?.label}}/>
                )
            case 'header':
                return (
                    <h4>{formElement?.label}</h4>
                )
            case 'number':
                const isNumberPopulated: boolean = formElement?.value != null;
                if (isNumberPopulated) {
                    return (
                        <div>
                            <InputLabel>{formElement?.label}</InputLabel>
                            {/* @ts-ignore */}
                            <TextField min={formElement?.min ? parseInt(formElement?.min?.toString()) : 0}
                                       type={"number"}
                                       name={formElement?.name}
                                       value={formElement?.value}
                                       className={formElement?.className}
                                       defaultValue={formElement?.value}
                                       required={formElement?.required}
                                       max={formElement?.max}
                                       step={formElement?.step}
                            />
                        </div>
                    )
                } else {
                    return (
                        <div>
                            <InputLabel>{formElement?.label}</InputLabel>
                            {/* @ts-ignore */}
                            <TextField min={formElement?.min ? parseInt(formElement?.min?.toString()) : 0}
                                       type={"number"}
                                       name={formElement?.name}
                                       value={formElement?.value}
                                       className={formElement?.className}
                                       required={formElement?.required}
                                       max={formElement?.max}
                                       step={formElement?.step}
                            />
                        </div>
                    )
                }
            default:
                Log.info(`Unaccounted form element type: ${formElement?.type}, skipping...`);
        }
    }

    return (
        <div>
            {
                // if the length of the setting array is zero display nothing
                setting?.length == 0
                    ? <></> :
                    <div className="row">
                        <div className="col">
                            <fieldset className="form-group border p-3">
                                <legend>{category}</legend>
                                {setting.map((formElement, index) =>
                                    <div className="form-group" key={index}>
                                        {buildFormElement(formElement)}
                                    </div>
                                )}
                            </fieldset>
                        </div>
                    </div>
            }
        </div>
    )
}