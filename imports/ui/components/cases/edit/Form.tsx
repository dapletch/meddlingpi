import React, {lazy, Suspense} from "react";
import {Cases} from "/imports/api/cases/cases";
import {Link} from "react-router-dom";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
import { Meteor } from "meteor/meteor";
// import {Session} from "meteor/session";
// import {SessionConstants} from "/imports/ui/constants/SessionConstants";
// @ts-ignore
import {Log} from "meteor/logging";

type Props = {
    caseRecord: Cases
}
export const Form = ({caseRecord}: Props) => {

    const utils = new CaseUtils();

    const GeneralSettings = lazy(() =>
        import('./Setting').then(({Setting}) => ({default: Setting}))
    );

    const SubjectSettings = lazy(() =>
        import('./Setting').then(({Setting}) => ({default: Setting}))
    );

    const AddressSettings = lazy(() =>
        import('./Setting').then(({Setting}) => ({default: Setting}))
    );

    const VehicleSettings = lazy(() =>
        import('./Setting').then(({Setting}) => ({default: Setting}))
    );

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const elements = event.currentTarget.elements;
        let inputs: any = [];
        for (let i: number = 0; i < elements.length; i++) {
            const element = elements[i];
            const type = element.attributes.getNamedItem('type')?.value;
            const name = element.attributes.getNamedItem('name')?.value;
            const className = element.className;
            if (type && name) {
                const nodes: NodeListOf<HTMLElement> = document.getElementsByName(name);
                inputs.push({
                    type: type,
                    name: name,
                    nodes: nodes
                });
                // dirty logic, but have to resort to this from the virtue of how the select tag is constructed in material-ui
            } else if (name && (name?.toLowerCase()?.includes('select') || className?.toLowerCase()?.includes('select'))) {
                const nodes: NodeListOf<HTMLElement> = document.getElementsByName(name);
                inputs.push({
                    type: 'select',
                    name: name,
                    nodes: nodes
                });
            }
        }
        // now filter the form to obtain a distinct list
        // documentation referenced:
        // https://stackoverflow.com/questions/43374112/filter-unique-values-from-an-array-of-objects
        let form: Array<any> = [];
        let general: Array<any> = [];
        let subjects: Array<any> = [];
        let addresses: Array<any> = [];
        let vehicles: Array<any> = [];
        // @ts-ignore
        inputs.filter(function(item) {
            // @ts-ignore
            let i = form.findIndex(x => x.name == item.name);
            if (i <= -1) {
                // add the element to the form collection
                form.push(item);
                if (item.name.startsWith('general')) {
                    general.push(item);
                } else if (item.name.startsWith('subjects')) {
                    subjects.push(item);
                } else if (item.name.startsWith('addresses')) {
                    addresses.push(item);
                } else if (item.name.startsWith('vehicles')) {
                    vehicles.push(item);
                } else {
                    Log.warn(`unrecognized form input: ${item.toString()}`)
                }
            }
            return null;
        });

        const utils = new CaseUtils();

        const revCaseRecord: any = {
            general: utils.updateCategory(general, caseRecord?.general),
            subjects: utils.updateCategory(subjects, caseRecord?.subjects),
            addresses: utils.updateCategory(addresses, caseRecord?.addresses),
            vehicles: utils.updateCategory(vehicles, caseRecord?.vehicles)
        }

        Log.debug(revCaseRecord);

        // call method method to update the data
        // @ts-ignore
        Meteor.call('cases.update', {_id: caseRecord?._id, caseRecord: revCaseRecord}, (err, res) => {
            if (!err) {
                Log.debug("update method successfully called...");

                // now add the current case into session
                caseRecord.general = revCaseRecord.general;
                caseRecord.subjects = revCaseRecord.subjects;
                caseRecord.addresses = revCaseRecord.addresses;
                caseRecord.vehicles = revCaseRecord.vehicles;

                utils.saveCaseInSession(caseRecord);
                utils.saveCaseSelection(caseRecord);

                // we've updated the record, but now we need to update the list
                // let cases: Array<Cases> = Session.get(SessionConstants.CASE_LIST);
                // const caseIndex = cases.map((e) => { return e.caseId; }).indexOf(caseRecord.caseId);
                // cases[caseIndex - 1] = caseRecord;
                // Session.set(SessionConstants.CASE_LIST, cases);

                // TODO replace with Bootstrap modal, but ok for now...
                alert(`You have just successfully updated Case - ${caseRecord.caseId}`);
            }
        });
    }

    const clearForm = () => {
        Array.from(document.querySelectorAll("input")).forEach(
            input => (input.value = "")
        );
        Array.from(document.querySelectorAll("input")).forEach(
            input => (input.checked = false)
        );
    };

    const hasAnySettingsAvailable: boolean =
        caseRecord?.general?.length > 0 || caseRecord?.subjects?.length > 0 ||
        caseRecord?.addresses?.length > 0 || caseRecord?.vehicles?.length > 0;

    return (
        <Suspense fallback={<div>Loading...</div>}>
            <h1 className={"text-center"}>Case - {caseRecord?.caseId}</h1>
            {hasAnySettingsAvailable
                ?
                <form onSubmit={handleSubmit} id={"case-form"}>
                    <GeneralSettings
                        category={'General'}
                        setting={utils.appendCategoryToFieldNames('general', caseRecord.general)}
                    />
                    <SubjectSettings
                        category={'Subjects'}
                        setting={utils.appendCategoryToFieldNames('subjects', caseRecord.subjects)}
                    />
                    <AddressSettings
                        category={'Addresses'}
                        setting={utils.appendCategoryToFieldNames('addresses', caseRecord.addresses)}
                    />
                    <VehicleSettings
                        category={'Vehicles'}
                        setting={utils.appendCategoryToFieldNames('vehicles', caseRecord.vehicles)}
                    />
                    <div className="row">
                        <div className="col text-right">
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                        <div className="col text-left">
                            <button type="button" className="btn btn-secondary" onClick={() => clearForm()}>Clear</button>
                        </div>
                    </div>
                </form>
                :
                <div className="alert alert-success alert-dismissible text-center">
                    <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                    The case you have selected currently has no settings. You can add/edit the settings for the case
                    {/* @ts-ignore TODO look into revising the language here... */}
                    by clicking <Link to={`/cases/settings/${caseRecord?._id}`}><b>here</b></Link>.
                </div>
            }
        </Suspense>
    );
}
