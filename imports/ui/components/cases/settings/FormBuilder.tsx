// jQuery formBuilder example provided by:
// https://formbuilder.online/docs/formBuilder/demos/react/

import {Session} from "meteor/session";
import $ from "jquery"; //Load jquery
import React, {Component} from "react";
import {Cases} from "/imports/api/cases/cases";
import {SessionConstants} from "/imports/ui/constants/SessionConstants";
import {SettingConstants} from "/imports/ui/constants/SettingConstants";
import {Meteor} from "meteor/meteor";
import {SettingsConstants} from "/imports/api/settings/constants";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
// @ts-ignore
import {Log} from "meteor/logging";

// @ts-ignore
window.jQuery = $; //JQuery alias
// @ts-ignore
window.$ = $; //JQuery alias

require("jquery");
require("jquery-ui-sortable"); //For FormBuilder Element Drag and Drop
require("formBuilder");// For FormBuilder

//Initialize formBuilder
export class FormBuilder extends Component {

    fb0 = React.createRef<HTMLDivElement>();
    fb1 = React.createRef<HTMLDivElement>();
    fb2 = React.createRef<HTMLDivElement>();
    fb3 = React.createRef<HTMLDivElement>();

    // @ts-ignore
    componentDidUpdate(prevProps: Readonly<{}>, prevState: Readonly<{}>, snapshot?: any) {
        // logic to clear out the pre-existing elements
        // when the user changes the case being viewed
        $("#fb-editor-0").html("");
        $("#fb-editor-1").html("");
        $("#fb-editor-2").html("");
        $("#fb-editor-3").html("");
        this.loadFormBuilderData();
    }

    componentDidMount() {
        // build the the form builders from the data in session
        this.loadFormBuilderData();
    }

    changeSetting(setting: string): void {
        Session.set(SessionConstants.CASE_SETTING, setting);
    }

    loadFormBuilderData(): void {

        let caseRecord: Cases = Session.get(SessionConstants.CASE_RECORD);

        if (caseRecord) {

            let config: any = [];

            // TODO pull from the database, but leave the SettingsConstants as the default value in else condition if database retrieval fails...
            const setting: any = SettingsConstants[SettingsConstants.length - 1];

            const formConfig = [
                caseRecord.general != null ? caseRecord.general : setting.general,
                caseRecord.subjects != null ? caseRecord.subjects : setting.subjects,
                caseRecord.addresses != null ? caseRecord.addresses : setting.addresses,
                caseRecord.vehicles != null ? caseRecord.vehicles : setting.vehicles
            ];

            for (let i = 0; formConfig.length > i; i++) {
                const options = {
                    dataType: 'json',
                    // disabling certain attributes for we'll manage that on our end while building the form
                    // https://formbuilder.online/docs/formBuilder/options/disabledAttrs/
                    disabledAttrs: [
                        'access',
                        'className',
                        'inline',
                        'maxlength',
                        'rows',
                        'subtype',
                        'other',
                        'toggle',
                        'multiple'
                    ],
                    // disabling certain form elements that can be used for the purposes of the formBuilder
                    // https://formbuilder.online/docs/formBuilder/options/disableFields/
                    disableFields: [
                        'autocomplete',
                        'button',
                        'file'
                    ],
                    formData: formConfig[i],
                    // onSave option enables custom functionality for the SAVE button rendered by formBuilder
                    // https://formbuilder.online/docs/formBuilder/options/onSave/
                    // @ts-ignore
                    onSave: function (evt: PointerEvent, formData: string) {
                        // if there hasn't bee a case setting selected yet, default to 'general'
                        const activeTab = Session.get(SessionConstants.CASE_SETTING) ?
                            Session.get(SessionConstants.CASE_SETTING) : SettingConstants.DEFAULT_SETTING;
                        // @ts-ignore
                        Meteor.call('cases.updateCaseSetting', {_id: caseRecord?._id, setting: activeTab, formData: JSON.parse(formData)}, (err, res) => {
                            if (!err) {
                                // update the case record
                                caseRecord = new CaseUtils().updateCaseRecord(activeTab, JSON.parse(formData), caseRecord);
                                // saved the revised object into session
                                Session.set(SessionConstants.CASE_RECORD, caseRecord);
                                // we've updated the record, but now we need to update the list
                                let cases: Array<Cases> = Session.get(SessionConstants.CASE_LIST);
                                const caseIndex = cases.map((e) => { return e.caseId; }).indexOf(caseRecord.caseId);

                                cases[caseIndex - 1] = caseRecord;
                                Session.set(SessionConstants.CASE_LIST, cases);
                                Session.set(SessionConstants.CASE_SETTING, SettingConstants.DEFAULT_SETTING);

                                // TODO make into a Bootstrap modal, but this works for now...
                                confirm(`Successfully updated the ${activeTab} setting in Case ${caseRecord.caseId}`);
                            } else {
                                Log.error(`An error occurred while updating the case setting ${err}`);
                            }
                        });
                    },
                };

                config.push(options);
            }

            // @ts-ignore
            $(this.fb0.current).formBuilder(config[0]);
            // @ts-ignore
            $(this.fb1.current).formBuilder(config[1]);
            // @ts-ignore
            $(this.fb2.current).formBuilder(config[2]);
            // @ts-ignore
            $(this.fb3.current).formBuilder(config[3]);

        } else {
            Log.info("Case not found. Not loading the formBuilder");
        }

    }

    render() {
        return (
            <div id="form-builder-block">
                <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li className="nav-item small" role="presentation">
                        <button className="nav-link active" id="pills-general-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-general"
                                type="button" role="tab" aria-controls="pills-general" aria-selected="true"
                                onClick={() => this.changeSetting('general')}>
                            General
                        </button>
                    </li>
                    <li className="nav-item small" role="presentation">
                        <button className="nav-link" id="pills-subjects-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-subjects"
                                type="button" role="tab" aria-controls="pills-subjects" aria-selected="false"
                                onClick={() => this.changeSetting('subjects')}>
                            Subjects
                        </button>
                    </li>
                    <li className="nav-item small" role="presentation">
                        <button className="nav-link" id="pills-addresses-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-addresses"
                                type="button" role="tab" aria-controls="pills-addresses" aria-selected="false"
                                onClick={() => this.changeSetting('addresses')}>
                            Addresses
                        </button>
                    </li>
                    <li className="nav-item small" role="presentation">
                        <button className="nav-link" id="pills-vehicles-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-vehicles"
                                type="button" role="tab" aria-controls="pills-vehicles" aria-selected="false"
                                onClick={() => this.changeSetting('vehicles')}>
                            Vehicles
                        </button>
                    </li>
                </ul>
                <div className="tab-content" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-general" role="tabpanel"
                         aria-labelledby="pills-general-tab">
                        <div id="fb-editor-0" ref={this.fb0}/>
                    </div>
                    <div className="tab-pane fade" id="pills-subjects" role="tabpanel"
                         aria-labelledby="pills-subjects-tab">
                        <div id="fb-editor-1" ref={this.fb1}/>
                    </div>
                    <div className="tab-pane fade" id="pills-addresses" role="tabpanel"
                         aria-labelledby="pills-addresses-tab">
                        <div id="fb-editor-2" ref={this.fb2}/>
                    </div>
                    <div className="tab-pane fade" id="pills-vehicles" role="tabpanel"
                         aria-labelledby="pills-vehicles-tab">
                        <div id="fb-editor-3" ref={this.fb3}/>
                    </div>
                </div>
            </div>
        )
    }

}