import React, {lazy, Suspense, useState} from "react";
import {CaseLayout} from "/imports/ui/components/layouts/CaseLayout";
import {Link, useNavigate, useParams} from "react-router-dom";
import {useTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Session} from "meteor/session";
import {CasesCollection} from "/imports/api/cases/cases";
import {SessionConstants} from "/imports/ui/constants/SessionConstants";
import {SettingConstants} from "/imports/ui/constants/SettingConstants";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
// @ts-ignore
import Select from 'react-select'
// @ts-ignore
import {Log} from "meteor/logging";

export const Settings = () => {

    const navigate = useNavigate();
    const [caseIndex, setCaseIndex] = useState(0);

    const utils = new CaseUtils();

    const FormBuilder = lazy(() =>
        import('./FormBuilder').then(({FormBuilder}) => ({default: FormBuilder}))
    );

    // unique identifier for the user's case
    const {id} = useParams();

    const caseRecord = useTracker(() => {
        Meteor.subscribe('cases.publication.caseRecord');
        let caseRecord = CasesCollection.findOne({_id: id, userId: Meteor.userId()})
        if (!caseRecord) {
            caseRecord = Session.get(SessionConstants.CASE_RECORD);
        }
        return caseRecord;
    });

    utils.saveCaseInSession(caseRecord);

    const cases = useTracker(() => {
        Meteor.subscribe('cases.publication.openCaseRecords');
        let cases = CasesCollection.find({userId: Meteor.userId(), isCaseClosed: false}).fetch();
        if (!cases) {
            cases = Session.get(SessionConstants.CASE_LIST)
        }
        return cases;
    });

    utils.saveCasesInSession(cases);

    const options = cases?.map(investigation => ({
        // @ts-ignore
        "value": investigation,
        "label": `Case - ${investigation.caseId}`
    }));

    // @ts-ignore
    const changeCaseSelection = (e) => {
        Log.debug("selection..." + e.value);
        setCaseIndex(utils.saveCaseSelection(e.value))
        // add the case into session so it can be retrieved by the FormBuilder when it gets updated
        Session.set(SessionConstants.CASE_RECORD, e.value)
        Session.set(SessionConstants.CASE_SETTING, SettingConstants.DEFAULT_SETTING);
        navigate(`/cases/settings/${e.value._id}`, {replace: true});
    }

    // call to set the default setting when the page gets refreshed
    utils.setDefaultSetting();

    const currentIndex: number = Session.get(SessionConstants.CASE_INDEX) ? Session.get(SessionConstants.CASE_INDEX) : 0;

    // need to have log statement for state of index to persist for some reason...
    Log.debug("currentIndex: " + currentIndex);

    return (
        <CaseLayout>
            <div className="container">
                <div className="row col m-3 text-center">
                    {
                        options ?
                            <Select options={options}
                                    defaultValue={options[currentIndex > 0 ? currentIndex: caseIndex]}
                                    onChange={changeCaseSelection.bind(this)}/> :
                            <Select options={options}
                                    onChange={changeCaseSelection.bind(this)}/>
                    }
                </div>
            </div>
            <Suspense fallback={<div>Loading...</div>}>
                <FormBuilder/>
            </Suspense>
            <div className="container">
                <div className="row justify-content-center m-4">
                    <div className="col-lg-12 col-md-12 col-sm-12 text-center">
                        {/* @ts-ignore */}
                        <Link className="btn btn-primary" to={`/cases/edit/${caseRecord?._id}`}>
                            Edit Case
                        </Link>
                    </div>
                </div>
            </div>
        </CaseLayout>
    );
}