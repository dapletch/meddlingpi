import React, {useState} from "react";
import {CaseLayout} from "/imports/ui/components/layouts/CaseLayout";
import {useTracker} from "meteor/react-meteor-data";
import {Meteor} from "meteor/meteor";
import {Cases, CasesCollection} from "/imports/api/cases/cases";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faRedo, faUndo, faFilePdf} from '@fortawesome/free-solid-svg-icons'
import {SessionConstants} from "/imports/ui/constants/SessionConstants";
import { Session } from "meteor/session";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
import axios from "axios";

export const List = () => {

    const [isCaseClosed, setIsCaseClosed] = useState(false);
    const utils = new CaseUtils();

    const cases = useTracker(() => {
        Meteor.subscribe('cases.publication.all', {isCaseClosed: isCaseClosed});
        return CasesCollection.find({isCaseClosed: isCaseClosed}).fetch();
    });

    const caseCount = useTracker(() => {
        Meteor.subscribe('cases.publication.count');
        return CasesCollection.find().count();
    });

    const createInitialCase = () => {
        if (isCaseClosed) {
            // toggle the navigation, so if the user is viewing closed cases they'll see cases being generated
            setIsCaseClosed(false);
            $('#caseStatusOpen').prop('checked', true);
        }
        Meteor.call('cases.create');
    }

    const deleteCase = (_id: string) => {
        Meteor.call('cases.delete', {_id: _id});
    }

    const saveCaseInfo = (caseRecord: Cases) => {
        // set the the case record and the index position into session
        Session.set(SessionConstants.CASE_RECORD, caseRecord);
        Session.set(SessionConstants.CASE_INDEX, utils.saveCaseSelection(caseRecord))
    }

    const setCaseStatus = (_id: string, isCaseClosed: boolean) => {
        Meteor.call('cases.setCaseStatus', _id, isCaseClosed);
    }

    const caseReportPdf = (caseRecord: Cases) => {
        // TODO remove headers when promoted to production, but needed here for local development
        const headers: any = {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
        }
        axios({
            method: 'post',
            headers: headers,
            // TODO make configurable
            url: 'https://sherlock-api.herokuapp.com/report/caseReportPdf',
            responseType: 'blob',
            data: {
                caseRecord: caseRecord
            }
        }).then(function (response) {
            // downloading file per: https://exceptionshub.com/download-pdf-from-http-response-with-axios-2.html
            window.open(URL.createObjectURL(response.data));
        });
    }

    return (
        <CaseLayout>
            {
                // only display when there are no cases to display
                caseCount == 0 ? (
                    <div className="alert alert-success alert-dismissible text-center">
                        <a href="#" className="close" data-dismiss="alert" aria-label="close">&times;</a>
                        You currently have no cases! To add a case click <b>Create Case</b> button to get started.
                    </div>
                ) : (
                    <></>
                )
            }

            {/* filter the cases by the isCaseClosed parameter */}
            <div className="toggle">
                <input id="caseStatusOpen" type="radio" name="caseStatus" value="open" defaultChecked={true}/>
                <label className="form-label" htmlFor="caseStatusOpen"
                       onClick={() => setIsCaseClosed(false)}>Open</label>
                <input id="caseStatusClosed" type="radio" name="caseStatus" value="closed"/>
                <label className="form-label" htmlFor="caseStatusClosed"
                       onClick={() => setIsCaseClosed(true)}>Closed</label>
            </div>

            {
                // only proceed with displaying records when there are records to display
                cases?.length == 0 ?
                    <></> :
                    <ul className="list-group">
                        {cases.map(investigation =>
                            // @ts-ignore
                            <li className="list-group-item case-item" id={investigation.caseId} key={investigation._id}>
                                <div className="d-flex media">
                                    <div className="media-body">
                                        <div className="d-flex media">
                                            <div className="media-body">
                                                <div className="row">
                                                    <div className="col-10">
                                                        <h5>
                                                            {/* @ts-ignore */}
                                                            <Link to={`/cases/edit/${investigation._id}`} onClick={() => saveCaseInfo(investigation)}>
                                                                <strong>Case {investigation.caseId}</strong>
                                                            </Link>
                                                        </h5>
                                                        <p>
                                                            {/* TODO look into adding back in the short description */}
                                                            {/*A brief description...<br/>*/}
                                                            {/* TODO look into alternative formats... */}
                                                            <small className="text-muted">{investigation?.recCreateTimestamp?.toDateString()} </small>
                                                        </p>
                                                    </div>
                                                    <div className="col-2">
                                                        <div className="dropdown pull-right">
                                                            <button
                                                                className="case-btn case-btn-link fa fa-chevron-down d-block w-100"
                                                                aria-expanded="false" data-bs-toggle="dropdown"
                                                                type="button">
                                                            </button>
                                                            <div className="dropdown-menu dropdown-menu-end">
                                                                {/* @ts-ignore */}
                                                                <a className="dropdown-item" onClick={() => !isCaseClosed ? setCaseStatus(investigation._id, true) : setCaseStatus(investigation._id, false)}>
                                                                    {!isCaseClosed ? <FontAwesomeIcon icon={faRedo}/>: <FontAwesomeIcon icon={faUndo}/>}
                                                                    {!isCaseClosed ? 'Close Case' : 'Reopen Case'}
                                                                </a>
                                                                <a className="dropdown-item" onClick={() => caseReportPdf(investigation)}>
                                                                    <FontAwesomeIcon icon={faFilePdf}/>
                                                                    Generate Report
                                                                </a>
                                                                {/* @ts-ignore */}
                                                                <Link className="dropdown-item" to={`/cases/edit/${investigation._id}`} onClick={() => saveCaseInfo(investigation)}>
                                                                    <div className="fa fa-pencil fa-fw"/>
                                                                    Edit Case
                                                                </Link>
                                                                {/* @ts-ignore */}
                                                                <Link className="dropdown-item" to={`/cases/settings/${investigation._id}`} onClick={() => saveCaseInfo(investigation)}>
                                                                    <div className="fa fa-cogs fa-fw"/>
                                                                    Edit Settings
                                                                </Link>
                                                                {/* TODO add Bootstrap Modal asking user they want to confirm whether they want to delete the case and all it's contents... */}
                                                                {/* @ts-ignore */}
                                                                <a className="dropdown-item" onClick={() => deleteCase(investigation._id)}>
                                                                    <div className="fa fa-trash-o fa-fw"/>
                                                                    Delete
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        )}
                    </ul>
            }

            <div className="row justify-content-center m-4">
                <div className="col-lg-12 col-md-12 col-sm-12 text-center">
                    <button className="btn btn-primary" onClick={createInitialCase}>Create Case</button>
                </div>
            </div>

            {/* TODO include at a later point in time after initial launch of the project */}
            {/*<nav aria-label="Pagination for cases">*/}
            {/*    <ul className="pagination justify-content-center">*/}
            {/*        <li className="page-item disabled">*/}
            {/*            <a className="page-link" href="#" aria-labelledby="First Page">First</a>*/}
            {/*            <span className="sr-only">First</span>*/}
            {/*        </li>*/}
            {/*        <li className="page-item">*/}
            {/*            <a className="page-link" href="#" aria-labelledby="Previous Page">&laquo;</a>*/}
            {/*            <span className="sr-only">Previous</span>*/}
            {/*        </li>*/}
            {/*        <li className="page-item"><a className="page-link" href="#">1</a></li>*/}
            {/*        <li className="page-item"><a className="page-link" href="#">2</a></li>*/}
            {/*        <li className="page-item"><a className="page-link" href="#">3</a></li>*/}
            {/*        <li className="page-item">*/}
            {/*            <a className="page-link" href="#" aria-labelledby="Next Page">&raquo;</a>*/}
            {/*            <span className="sr-only">Next</span>*/}
            {/*        </li>*/}
            {/*        <li className="page-item">*/}
            {/*            <a className="page-link" href="#" aria-labelledby="Previous Page">Last</a>*/}
            {/*            <span className="sr-only">Last</span>*/}
            {/*        </li>*/}
            {/*    </ul>*/}
            {/*</nav>*/}
        </CaseLayout>
    );
}