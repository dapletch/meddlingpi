import React from "react";

import {CaseLayout} from "/imports/ui/components/layouts/CaseLayout";

export const Create = () => {
    return (
        <CaseLayout>
            <p>Create Case Page...</p>
        </CaseLayout>
    );
}