/**
 * Used to retrieve the records from the Meteor Session
 */
export class SessionConstants {

    public static CASE_RECORD: string = "CASE_RECORD";

    public static CASE_LIST: string = "CASE_LIST";

    public static CASE_SETTING: string = "CASE_SETTING";

    public static CASE_INDEX: string = "CASE_INDEX";

}