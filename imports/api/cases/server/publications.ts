import { Meteor } from "meteor/meteor";
import {CasesCollection} from "/imports/api/cases/cases";

Meteor.publish('cases.publication.all', (query: object) => {
    // TODO add session to account for admin roles
    if (!Meteor.userId()) {
        return new Meteor.Error("cases.publication.userId", "User session not populated. Please sign in and try again...");
    }
    return CasesCollection.find(query);
});

Meteor.publish('cases.publication.count', () => {
    // TODO add session to account for admin roles
    if (!Meteor.userId()) {
        return new Meteor.Error("cases.publication.count.userId", "User session not populated. Please sign in and try again...");
    }
    return CasesCollection.find();
});

Meteor.publish('cases.publication.caseRecord', (_id: string) => {
    // TODO add session to account for admin roles
    if (!Meteor.userId()) {
        return new Meteor.Error("cases.publication.caseRecord.userId", "User session not populated. Please sign in and try again...");
    }
    return CasesCollection.findOne({_id: _id, userId: Meteor.userId()});
});

Meteor.publish('cases.publication.openCaseRecords', () => {
    // TODO add session to account for admin roles
    if (!Meteor.userId()) {
        return new Meteor.Error("cases.publication.openCaseRecords.userId", "User session not populated. Please sign in and try again...");
    }
    return CasesCollection.find({userId: Meteor.userId(), isCaseClosed: false});
});