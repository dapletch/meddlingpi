import { Mongo } from 'meteor/mongo';

export interface Cases {
    userId: string | null;
    caseId: number;
    recCreateUser: string | undefined;
    recCreateTimestamp: Date;
    recChangeUser: string | undefined;
    recChangeTimestamp: Date | undefined;
    isCaseClosed: boolean;
    general: Array<object>,
    subjects: Array<object>,
    addresses: Array<object>,
    vehicles: Array<object>
}

export const CasesCollection = new Mongo.Collection<Cases>('cases');