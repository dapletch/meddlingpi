import {Meteor} from 'meteor/meteor';
import {CasesCollection} from "/imports/api/cases/cases";
import {check} from "meteor/check";
import {SettingsCollection} from "/imports/api/settings/settings";
import {SettingsConstants} from "/imports/api/settings/constants";
import {CaseUtils} from "/imports/ui/utils/CaseUtils";
// @ts-ignore
import { Log } from "meteor/logging";


function getSettingCount(): number {
    if (!Meteor.userId()) {
        throw new Meteor.Error("getSettingCount", "User is not logged in, please login and try again...");
    } else {
        let count: number = 0;
        if (Meteor.isServer) {
            count = SettingsCollection.find().count();
            count = count > 0 ? count - 1 : 0;
        }
        return count;
    }
}

function getCurrentCaseId(userId: string): number {
    if (!Meteor.userId()) {
        throw new Meteor.Error("getCurrentCaseId", "User is not logged in, please login and try again...");
    } else {
        // checking the userId
        check(userId, String)
        let caseId: number = 0;
        if (Meteor.isServer) {
            // selecting the maximum case if possible to increment current max value by one
            const caseByMaxCaseId: any = CasesCollection.findOne({userId: userId}, {sort: {caseId: -1}});
            caseId = !caseByMaxCaseId ? 0 : caseByMaxCaseId.caseId + 1;
        }
        return caseId;
    }
}

Meteor.methods({
    'cases.create'() {
        if (Meteor.isServer) {
            // securing method strictly to user
            if (!this.userId) {
                throw new Meteor.Error("cases.create.userId", "User is not logged in, please login and try again...");
            }

            let setting: any = SettingsCollection.findOne({version: getSettingCount()});
            // added conditional just in case...
            if (!setting) {
                // get max value from SettingsConstants in the event query fails...
                setting = SettingsConstants[SettingsConstants.length - 1];
            }

            const utils = new CaseUtils();

            // now proceed with inserting case
            CasesCollection.insert({
                userId: this.userId,
                caseId: getCurrentCaseId(this.userId),
                recCreateUser: this.userId,
                recCreateTimestamp: new Date(),
                recChangeTimestamp: undefined,
                recChangeUser: undefined,
                isCaseClosed: false,
                general: utils.appendCategoryToFieldNames('general', setting.general),
                subjects: utils.appendCategoryToFieldNames('subjects', setting.subjects),
                addresses: utils.appendCategoryToFieldNames('addresses', setting.addresses),
                vehicles: utils.appendCategoryToFieldNames('vehicles', setting.vehicles)
            });
        } else {

        }
    },
    'cases.update'(args: any) {
        if (Meteor.isServer) {
            if (!this.userId) {
                throw new Meteor.Error("cases.update.userId", "User is not logged in, please login and try again...");
            }
            Log.debug(args);
            // validate the input arguments being sent
            check(args?._id, String);
            check(args?.caseRecord, Object);

            const _id: string = args?._id;
            const updates: any = args?.caseRecord;

            const caseRecord: any = CasesCollection.findOne({_id: _id, userId: this.userId})
            if (caseRecord) {
                Log.debug("case found...");
                CasesCollection.update(
                    {_id: _id, userId: this.userId},
                    {
                        $set: {
                            recChangeUser: this.userId,
                            recChangeTimestamp: new Date(),
                            general: updates?.general ? updates.general : caseRecord.general,
                            subjects: updates?.subjects ? updates.subjects : caseRecord.subjects,
                            addresses: updates?.addresses ? updates.addresses : caseRecord.addresses,
                            vehicles: updates?.vehicles ? updates.vehicles : caseRecord.addresses,
                        }
                    }
                );
            } else {
                Log.info(`Case not found user: ${this.userId} with _id: ${_id}`);
            }
        }
    },
    'cases.updateCaseSetting'(args: any) {
        if (Meteor.isServer) {
            if (!this.userId) {
                throw new Meteor.Error("cases.updateCaseSetting.userId", "User is not logged in, please login and try again...");
            }

            // validating the data...if invalid exception is thrown
            check(args?._id, String);
            check(args?.setting, String);
            check(args?.formData, Array);

            const _id: string = args?._id;
            const setting: string = args?.setting;
            const formData: Array<any> = args?.formData;

            const caseRecord: any = CasesCollection.findOne({_id: _id, userId: this.userId});
            // determine if record exists before updating
            if (caseRecord) {

                const user = Meteor.user();
                let updates: any = {
                    recChangeUser: user?._id,
                    recChangeTimestamp: new Date()
                };

                let hasSettingBeenUpdated: boolean = false;
                const utils = new CaseUtils();

                switch (setting) {
                    case 'general':
                        updates.general = utils.appendCategoryToFieldNames(setting, formData);
                        hasSettingBeenUpdated = true;
                        break;
                    case 'subjects':
                        updates.subjects = utils.appendCategoryToFieldNames(setting, formData);
                        hasSettingBeenUpdated = true;
                        break;
                    case 'addresses':
                        updates.addresses = utils.appendCategoryToFieldNames(setting, formData);
                        hasSettingBeenUpdated = true;
                        break;
                    case 'vehicles':
                        updates.vehicles = utils.appendCategoryToFieldNames(setting, formData);
                        hasSettingBeenUpdated = true;
                        break;
                    default:
                        Log.error("An invalid setting is attempting to be updated: " + formData);
                }

                // proceed if the object has keys, as in has been populated
                if (hasSettingBeenUpdated) {
                    CasesCollection.update(
                        {_id: _id, userId: this.userId},
                        {
                            $set: {
                                recChangeUser: updates.recChangeUser,
                                recChangeTimestamp: updates.recChangeTimestamp,
                                general: updates?.general ? updates.general : caseRecord.general,
                                subjects: updates?.subjects ? updates.subjects : caseRecord.subjects,
                                addresses: updates?.addresses ? updates.addresses : caseRecord.addresses,
                                vehicles: updates?.vehicles ? updates.vehicles : caseRecord.addresses,
                            }
                        }
                    );
                } else {
                    Log.info(`Invalid setting passed, not updating the data: ${setting}`);
                }
            } else {
                Log.info(`Case not found user: ${this.userId} with _id: ${_id}`);
            }
        } else {
            Log.error("cases.updateCaseSetting() method not called on server.");
        }
    },
    // method called like so:
    // Meteor.call('cases.delete', {_id: ''})
    // or Meteor.call('cases.delete', {caseId: 0})
    // @ts-ignore
    'cases.delete'(args: any) {
        if (Meteor.isServer) {
            if (!this.userId) {
                throw new Meteor.Error("cases.delete.userId", "User is not logged in, please login and try again...");
            }

            // now remove the case by using the _id
            if (args?._id) {
                // added per: https://docs.meteor.com/api/check.html
                check(args?._id, String);
                Log.info(`Removing case by _id: ${args._id}`);
                // now determine if the cases actually exists
                const caseByObjectId: any = CasesCollection.findOne({_id: args?._id, userId: this.userId});
                if (caseByObjectId) {
                    CasesCollection.remove({_id: caseByObjectId._id});
                } else {
                    Log.info(`Case not found with _id provided: ${args._id}`);
                }
            }

            // alternatively we can delete by using the userId and caseId, if caseId is provided of course...
            if (args?.caseId) {
                // added per: https://docs.meteor.com/api/check.html
                check(args.caseId, Number)
                Log.info(`Removing case by caseId: ${args.caseId}`);
                const caseByCaseId: any = CasesCollection.findOne({userId: this.userId, caseId: args.caseId});
                if (caseByCaseId) {
                    CasesCollection.remove({_id: caseByCaseId._id});
                } else {
                    Log.info(`Case not found with caseId provided: ${args.caseId}`);
                }
            }
        } else {
            Log.error("cases.delete() method called not on thee server.");
        }
    },
    'cases.setCaseStatus'(_id: string, isCaseClosed: boolean) {
        if (Meteor.isServer) {
            if (!this.userId) {
                throw new Meteor.Error("cases.setCasesStatus.userId", "User is not logged in, please login and try again...");
            }
            const userCase: any =  CasesCollection.findOne({_id: _id, userId: this.userId});
            if (!userCase) {
                throw new Meteor.Error("cases.setCaseStatus.caseNotFound", "User case was not found...");
            }
            // now mark the case as closed
            CasesCollection.update(
                {_id: _id, userId: this.userId},
                {
                    $set: {
                        isCaseClosed: isCaseClosed
                    }
                }
            );
        } else {
            Log.error("cases.setCaseStatus() method no called on the server.");
        }
    }
})
