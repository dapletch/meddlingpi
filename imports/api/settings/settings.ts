import { Mongo } from 'meteor/mongo';

export interface Settings {
    version: number;
    dateCreated: Date;
    general: Array<object>,
    subjects: Array<object>,
    addresses: Array<object>,
    vehicles: Array<object>
}

// used to store the default settings, so they are database driven as opposed to configuration based
export const SettingsCollection = new Mongo.Collection<Settings>('settings');