import {Meteor} from 'meteor/meteor';
// @ts-ignore
import {Log} from "meteor/logging";

// method used for user CRUD operations...
Meteor.methods({
    // only include @ts-ignore when adding method signature
    'user.logLogin'() {
        // TODO look into possibly storing user login information into collection
        Log.info("userId: " + this.userId)
    }
});